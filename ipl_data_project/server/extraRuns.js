const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module

const matches = []; //created empty array to store matches data
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => matches.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    const deliveries = []; //at end of task created new empty array to store deliveries
    fs.createReadStream("../data/deliveries.csv")   
      .pipe(csv())  
      .on("data", (data) => deliveries.push(data))  
      .on("error", (error) => console.log(error))
      .on("end", () => {
        extraRuns(matches, deliveries, 2016); //calling the function
      });
  });

function extraRuns(matchData, deliveriesData, year) {
  //created function
  const matchId = []; //created array
  for (let value of matchData) {
    if (value.hasOwnProperty("id")) {
      if (value.season == year) {
        //checking if year is 2016
        matchId.push(value.id); //pushing match id in array
      }
    }
  }
  const extraRunsPerTeam = {}; //new object created
  for (let value of deliveriesData) {
    if (matchId.includes(value.match_id)) {
      //if matchId is present in our array
      if (extraRunsPerTeam.hasOwnProperty(value.bowling_team)) {
        //if bowling team is present in object
        let extra = Number(value.extra_runs);
        extraRunsPerTeam[value.bowling_team] += extra; //adding extra runs to particular team
      } else {
        extraRunsPerTeam[value.bowling_team] = Number(value.extra_runs); //else created new property and added runs
      }
    }
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/extrasPerTeam.json",
    JSON.stringify(extraRunsPerTeam), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
