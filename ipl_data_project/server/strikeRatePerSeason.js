const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module
const matches = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => matches.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    const deliveries = []; //created empty array to store deliveries data
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())  
      .on("data", (data) => deliveries.push(data))
      .on("error", (error) => console.log(error))
      .on("end", () => {
        strikeRatePerSeason(matches, deliveries); //calling function   
      });   
  });

function strikeRatePerSeason(matches, deliveries) {
  //created function
  const seasons = {};
  for (let match of matches) {
    if (match.hasOwnProperty("season") && match.hasOwnProperty("id")) {
      //match has season and id property or not
      const year = match.season; //assigned year
      if (seasons.hasOwnProperty(year) === false) {
        //if object does'nt contain year
        seasons[year] = []; //created property and assigned empty array
      }
      seasons[year].push(match.id); //pushed ids with respect to it's year
    }
  }

  const results = {};
  for (let year in seasons) {
    for (let delivery of deliveries) {
      if (seasons[year].includes(delivery.match_id)) {
        //if match id includes in on of the year
        const batsman = delivery.batsman; // assigning batsman name
        if (results.hasOwnProperty(batsman) === false) {
          //if name does'nt exist
          results[batsman] = {}; //created property
        }
        if (results[batsman].hasOwnProperty(year) === false) {
          //if year is not present
          results[batsman][year] = {
            //created year and added runs and balls
            runs: Number(delivery.batsman_runs),
            balls: 1,
          };
        } else {
          //updating balls and runs
          results[batsman][year].runs += Number(delivery.batsman_runs);
          results[batsman][year].balls++;
        }
      }
    }
  }
  const strikeRate = findStrikeRate(results); //calling function to calculate strike rate
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/strikeRate.json", //path where file should create
    JSON.stringify(strikeRate), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}

function findStrikeRate(results) {
  //created function
  const strikeRate = {};
  for (let player in results) {
    //iterating inside results object
    strikeRate[player] = {};
    for (let year in results[player]) {
      //iterating in every season of a player
      let run = results[player][year].runs;
      let ball = results[player][year].balls;
      strikeRate[player][year] = { strikeRate: (run / ball) * 100 }; //calculating strike rate and storing in object
    }
  }
  return strikeRate;
}
