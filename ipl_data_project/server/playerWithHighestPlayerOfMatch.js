const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module
const matches = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => matches.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    playerWithHigestPlayerOfMatch(matches); //calling function
  });

function playerWithHigestPlayerOfMatch(matchData) {
  //created function
  const playerOfTheMatch = {}; //created object   
  for (let value of matchData) {
    if (playerOfTheMatch.hasOwnProperty(value.season)) { 
      //if year is present  
      if (
        playerOfTheMatch[value.season].hasOwnProperty(value.player_of_match) //player name present
      ) {
        playerOfTheMatch[value.season][value.player_of_match]++; //incrementing no. of mom
      } else {
        playerOfTheMatch[value.season][value.player_of_match] = 1; // adding 1 to no. of mom
      }
    } else {
      //if year and player name is not present
      playerOfTheMatch[value.season] = {}; //added year and assigned empty object
      playerOfTheMatch[value.season][value.player_of_match] = 1; //added player name and assigned with 1
    }
  }
  const higestManOfTheMatch = {}; //created object
  for (let value in playerOfTheMatch) {
    const player = playerOfTheMatch[value]; //assigned details of a season in new object
    let maxWinner = 0; //created a variable and assigned value as o for reference
    let maxPlayer = ""; //created a variable
    for (let players in player) {
      const winner = player[players]; //asigning no. of time mom for a player
      if (winner > maxWinner) {
        //if winner is greater than maxWinner
        maxWinner = winner; //assigning values
        maxPlayer = players;
      } else if (winner === maxWinner) {
        //if both values are equal
        maxWinner = winner;
        maxPlayer = maxPlayer + ", " + players; //assigning both names
      }
    }
    higestManOfTheMatch[value] = {
      //atleast adding values in our object
      player: maxPlayer,
      manOfTheMatch: maxWinner,
    };
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/highestManOfMatch.json", //path where file should create
    JSON.stringify(higestManOfTheMatch), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
