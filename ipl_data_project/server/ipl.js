const express = require("express");
const path = require("path");
const app = express();
const port = 3000;

app.get("/output/:filename", (req, res) => {
  const filename = req.params.filename; //getting file name parameter
  const filePath = path.join(__dirname, "../public", "output", filename); //creating filePath
  res.sendFile(filePath); //sending file to client as response
});   

app.get("/result/:filename", (req, res) => {
  const filename = req.params.filename; //getting file name parameter
  const filePath = path.join(__dirname, "../public", "problemHtml", filename); //creating filePath
  res.sendFile(filePath); //sending file to client as response  
});

app.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`); //print url  
});
