const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module

const result = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => {
    //adding operation in particular event
    result.push(data);
  })
  .on("error", (error) => {  
    console.log(error);
  })
  .on("end", () => {
    matchWonPerTeam(result); //calling function    
  });  

function matchWonPerTeam(arrayOfMatches) {
  //created function
  const matchesWon = {}; //created object
  for (let year of arrayOfMatches) {
    if (matchesWon.hasOwnProperty(year.season)) {
      //if object has particular year
      if (matchesWon[year.season].hasOwnProperty(year.winner)) {
        //and year in the object has team name
        matchesWon[year.season][year.winner]++; //incrementing value by 1
      } else {
        //if team is not present
        matchesWon[year.season][year.winner] = 1; //added team and assigned value
      }
    } else {
      //if year is not present in object
      matchesWon[year.season] = {}; //created year property and assigned empty object
      matchesWon[year.season][year.winner] = 1; //added team name and assigned 1
    }
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/matchesWonPerTeam.json",
    JSON.stringify(matchesWon), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
