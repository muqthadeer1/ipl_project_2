const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module
const deliveries = []; //created empty array to store deliveries data
fs.createReadStream("../data/deliveries.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => deliveries.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    playerDismissedPlayer(deliveries); //calling function
  });    

function playerDismissedPlayer(deliveries) {
  let summary = {}; //created empty object
  for (let value of deliveries) {
    //iterating inside deliveries
    if (value.player_dismissed) {  
      //if player dissmissed in the ball condition will be satisfied  
      if (summary.hasOwnProperty(value.bowler) === false) {
        //if bowler name is not present in object
        summary[value.bowler] = {}; //it will create property with bowler name and assign empty object
        summary[value.bowler][value.batsman] = 1; //it will create property batsMan name and assign number of dissmissals
      } else {
        //if bowler name already present
        if (summary[value.bowler].hasOwnProperty(value.batsman) === false) {
          //if batsman name is not present
          summary[value.bowler][value.batsman] = 1; //creating property with batsman name and adding 1
        } else {
          //if both bowler name and batsman name are present
          summary[value.bowler][value.batsman]++; //incrementing dissimals
        }
      }
    }
  }
  let highest = Number.MIN_VALUE; //created a variable assigned lowest number variable
  let result = {}; //created empty object
  let bowler = Object.keys(summary); //retrived all keys of object
  for (let key of bowler) {
    //iterating inside array
    for (let innerkey in summary[key]) {
      //interating inside bowler object
      let dissmiss = summary[key][innerkey]; //assigned number of dissmissals
      if (highest < dissmiss) {
        highest = dissmiss; //assigning dissmiss value to highest variable
        result = { bowler: key, batsman: innerkey, dissmissals: dissmiss }; //assigning value inside object
      }
    }
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/playerDissmissal.json", //path where file should create
    JSON.stringify(result), //converting object to json type
    (error) => {
      //if error occurs it will print it
      console.log(error);
    }
  );
}
