const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module

const result = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => {
    //adding operation in particular event
    result.push(data);
  })
  .on("error", (error) => {
    console.log(error);
  })
  .on("end", () => {
    wonTossAndMatch(result); //calling function   
  });     

function wonTossAndMatch(matchData) {
  //created function
  const winningTeam = {};
  for (let value of matchData) {
    if (value.hasOwnProperty("toss_winner") && value.hasOwnProperty("winner")) {
      if (value.toss_winner === value.winner) {
        //toss and match winner are same
        if (winningTeam.hasOwnProperty(value.toss_winner)) {
          //if team already exists
          winningTeam[value.toss_winner]++; //incrementing value
        } else {
          winningTeam[value.toss_winner] = 1; //added team name assigned value as 1
        }
      }
    }
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/wonTossAndMatch.json", //path where file should create
    JSON.stringify(winningTeam), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
