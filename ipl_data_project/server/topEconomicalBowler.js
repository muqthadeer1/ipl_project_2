const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module
const matches = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => matches.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    const deliveries = []; //created empty array
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())
      .on("data", (data) => deliveries.push(data))
      .on("error", (error) => console.log(error))  
      .on("end", () => {
        topEconomicBowler(matches, deliveries, 2015); //calling function  
      });
  });   

function topEconomicBowler(matchData, deliveriesData, year) {
  //created function
  const matchId = [];
  for (let value of matchData) {
    if (value.hasOwnProperty("id") && value.hasOwnProperty("season")) {
      if (value.season == year) {
        matchId.push(value.id); //pushing id of particular year
      }
    }
  }

  let economical = {}; //created object
  for (let value of deliveriesData) {
    //iterating inside array
    //checking if following properties are present or not
    if (matchId.includes(value.match_id)) {
      //if id includes than stores data
      if (economical.hasOwnProperty(value.bowler) === false) {
        economical[value.bowler] = { balls: 1, runs: Number(value.total_runs) };
      } else {
        economical[value.bowler].balls++;
        economical[value.bowler].runs += Number(value.total_runs);
      }
    }
  }
  const bowlerEconomy = {};
  for (let player in economical) {
    bowlerEconomy[player] = parseFloat(
      (economical[player].runs / (economical[player].balls / 6)).toFixed(2) //calculating economy
    );
  }
  const sortedEconomy = Object.entries(bowlerEconomy).sort(
    //sorting
    ([, value1], [, value2]) => {
      return value1 - value2;
    }
  );
  const results = sortedEconomy.slice(0, 10); //assigned first 10 values

  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/topEconomicalBowler.json", //path where file should create
    JSON.stringify(results), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
