const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module
const deliveries = []; //created empty array to store deliveries data
fs.createReadStream("../data/deliveries.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => deliveries.push(data)) //adding operation in particular event
  .on("error", (error) => console.log(error))
  .on("end", () => {
    //console.log(deliveries);  
    bestEconomyInSuperOver(deliveries); //calling function   
  });  

function bestEconomyInSuperOver(deliveries) {
  //created function
  const bowlers = {}; //created empty function
  for (let value of deliveries) {
    if (value.is_super_over == 1) {
      //checking if over is suer over or not
      if (bowlers.hasOwnProperty(value.bowler) === false) {
        //if object does'nt have property
        bowlers[value.bowler] = { ball: 1, runs: Number(value.total_runs) }; //adding property bowler name and  number of balls and runs
      } else {
        bowlers[value.bowler].ball++; //already have bowler than incrementing ball
        bowlers[value.bowler].runs += Number(value.total_runs); //adding runs
      }
    }
  }
  let minimumEconomy = Number.MAX_VALUE; //maximum number value
  let bestEconomy = {}; //created empty funtion
  for (let value in bowlers) {
    if (bowlers[value].hasOwnProperty("overs") == false) {
      //checking if we have overs or not
      bowlers[value] = {
        overs: bowlers[value].ball / 6, //adding new property overs and adding value to it
        runs: bowlers[value].runs,
      };
      if (bowlers[value].hasOwnProperty("economy") === false) {
        //add economy
        bowlers[value] = {
          economy: bowlers[value].runs / bowlers[value].overs, //calculating economy
        };
      }
      if (bowlers[value].economy < minimumEconomy) {
        //if economy is less than minimumEconomy
        bestEconomy = { bowler: value, economy: bowlers[value].economy }; //adding bowler and economy to object
        minimumEconomy = bowlers[value].economy; //updating minimumEconomy
      }
    }
  }

  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/bestBowlerInSuperOver.json",
    JSON.stringify(bestEconomy), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
